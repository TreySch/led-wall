#include <TimerThree.h>
#include <SPI.h>
#include <CapacitiveSensor.h>

/*  NOTES:
    >>  Touch values have a time out of 1 millisecond - think about adding a setting to extend time?
    >>  Display function could be used to constantly update (may cause flickering and slow performance) or have dependent
        function that needs to be called (requires programmer to be mindful of updating the display)
    >>  Vitual Joysticks or controls in the same rows or colomns will be difficult to differentiate SO PLEASE AVOID IT!
    >>  Pin declarations assume use of Teensy 3.2 ↓
        https://www.pjrc.com/teensy/card7a_rev1.png pinout
    >>  NEEDS TO BE TESTED -- what refresh time works best for the update function
        https://www.pjrc.com/teensy/td_libs_TimerOne.html
    >>  Global variables use less mamory so try to use those inplace of local variables as often as possible
    >>  When reading the touchmat be sure to turn off the interupt before reading the matrix and turning it back on afterwards
        EX:
        noInterrupts();
        test = touchmat[y][x];
        interrupts();

    >>  Look into changing touchmat into an int matrix by verifying that unsigned 8bit won't be large enough
    >>  Touchmat "update" function nees to be optimized
    >>  Next iteration idea: standardize a beginning bit that signifies the first/last byte of three
    >>  Game board can be scaled as 1x, 2x, or 3x
*/

//-=-=-=-=-=-=- Core Settings -=-=-=-=-=-=-=-=-
#define TSwidth 18 //Actual x resolution of the screen
#define TSheight 27 //Actual y resolution of the screen
#define gameboardx 36 //Resolution of the game board
#define gameboardy 54 //Resolution of the game board
#define buttonnum 6 //number of buttons on exhibit
#define DEBUG 1 //used for debugging the game
#define btsco 4000 //button touch sense cut off used for determining whether the button has been touched
//-=-=-=-=-=-=- Game Settings -=-=-=-=-=-=-=-=-
//    ...........................................................................................................................   ENTER SETTINGS HERE

//-=-=-=-=-=-=-=-=-  Pins -=-=-=-=-=-=-=-=-=-=-
//Outputs
#define indLED 2 //used to show that the button connected properly
#define requestpin 3 //active high that indicates that someone has 
#define redLED 14 //causes the red onboard LEDs to illuminate
#define greenLED 15 //causes the green onboard LEDs to illuminate
#define blueLED 16 //causes the blue onboard LEDS to illuminate
//Inputs
#define brightpin A4 //used to adjust the brightness
#define positionpin A9 //used to measure where the button is placed
#define demopin 20 //active high that tells button to run demo function
#define runpin 19 //active high that acknowledges and approves the game to run
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



//-=-=-=-=-=-=-=- Sounds -=-=-=-=-=-=-=-=-=-=-
const int sounds[16][3] = {
  /* Can play multiple notes and instruments at once
     Feel Free to edit sounds
     pitch, velocity, instrument
     {0x00-0x7F, 0x00-0x7F, 0x00-0x7F}*/
  {81, 0x64, 114}, //
  {83, 0x64, 114}, //
  {84, 0x64, 114}, //
  {86, 0x64, 114}, //
  {88, 0x64, 114}, //
  {89, 0x64, 114}, //
  {94, 0x64, 114}, //
  {95, 0x64, 114}, //
  {57, 0x64, 29}, //
  {59, 0x64, 29}, //
  {60, 0x64, 29}, //
  {62, 0x64, 29}, //
  {64, 0x64, 29}, //
  {65, 0x64, 29}, //
  {67, 0x64, 29}, //
  {69, 0x64, 29}  //
};
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



//-=-=-=-=-=-=-=- Cololrs -=-=-=-=-=-=-=-=-=-=-
const int colrs[16][4] = {
  /* Uses color addition
     Feel Free to edit colors
     brightness, blue, green, red
     {0x00-0x1F, 0x00-0xFF, 0x00-0xFF, 0x00-0xFF}*/
  {0x00, 0x00, 0x00, 0x00}, //Black
  {0x1F, 0xFF, 0xFF, 0xFF}, //White
  {0x1F, 0xFF, 0x00, 0x00}, //Blue
  {0x1F, 0x00, 0xFF, 0x00}, //Green
  {0x1F, 0x00, 0x00, 0xFF}, //Red
  {0x1F, 0xFF, 0xFF, 0x00}, //Cyan
  {0x1F, 0x00, 0xFF, 0xFF}, //Yellow
  {0x1F, 0xFF, 0x00, 0xFF}, //Magenta
  {0x1F, 0x00, 0x7F, 0xFF}, //Orange
  {0x1F, 0x4F, 0x4F, 0xFF}, //Pink
  {0x1F, 0x7F, 0x00, 0xFF}, //Red-Purple
  {0x1F, 0xFF, 0x00, 0x7F}, //Blue-Purple
  {0x1F, 0x00, 0xFF, 0x7F}, //Yellow-Green
  {0x1F, 0x7F, 0xFF, 0x00}, //Blue-Green
  {0x10, 0x7F, 0x7F, 0x7F}, //Gray
  {0x03, 0x20, 0x47, 0x7E}  //Brown
};
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



//-=-=-=-=-=- Global Variables -=-=-=-=-=-=-=-
bool demo = 0;
bool request = 0;
bool run = 0;
volatile uint8_t touchmat[TSheight][TSwidth]; // updated using timer interupt
unsigned long int scaleddispmat[TSheight][TSwidth];
int gameboard[gameboardy][gameboardx];
uint8_t xloc = -1;
uint8_t yloc = -1;
uint8_t tvalue = -1;
unsigned long prevmil; //used for time out
const float difxdisp =  gameboardx / TSwidth; //used for display update function
const float difydisp =  gameboardy / TSheight; //used for display update function
int dispscaling; //used to simplify calculations
int upscale = 0; //used to simplify calculations
int downscale = 0; //used to simplify
float bright = 0; //
int scred;
int scgreen;
int scblue;
int scbright;
int scloc;
int dx = 0;
int dy = 0;
//    ...........................................................................................................................   ENTER GLOBAL VARIABLES HERE

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



//-=-=-=-=-=-=-=-=-  OOP  -=-=-=-=-=-=-=-=-=-=-
/*
  class TouchMatrixUpdate
  {
    uint8_t xloc = -1;
    uint8_t yloc = -1;
    uint8_t tvalue = -1;
    unsigned long prevmil; //used for time out

  public:
    TouchMatrixUpdate(void)
    {
      Timer3.initialize(2000); //sets to run every 2000usec
      Timer3.attachInterrupt(update);
      Serial1.begin(19200);
      for (int tx = 0; tx < TSwidth; tx++) {
        for (int ty = 0; ty < TSheight; ty++) {
          touchmat[ty][tx] = 0;
        }
      }
    }

  private:
    void update(void)
    {
      //__________________________________  needs to be optimized!!!
      prevmil = millis();
      do {
        yloc = (uint8_t) Serial1.read();
      } while ((yloc == -1) | (prevmil == millis()));
      do {
        xloc = (uint8_t) Serial1.read();
      } while ((xloc == -1) | (prevmil == millis()));
      do {
        tvalue = (uint8_t) Serial1.read();
      } while ((tvalue == -1) | (prevmil == millis()));
      if (prevmil == millis()) {
        touchmat[yloc][xloc] = tvalue;
      }
    }
  };
*/
//    ...........................................................................................................................   ENTER C++ HERE


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-




//-=-=-=-=- OOP Initializations -=-=-=-=-=-=-=-
//TouchMatrixUpdate touch;
CapacitiveSensor   cs_4_6 = CapacitiveSensor(4, 6);
//    ...........................................................................................................................   ENTER C++ Initializations HERE


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-




//-=-=-=-=- Function Declarations -=-=-=-=-=-=-
void ledsOn(void);
void ledsOff(void);
void dvs(void); //demo variable set
void rvs(void); //run variable set
void game(void);
void demofunction(void);
void noteDrums(int, int);
void noteOff(int, int);
void noteOn(int, int, int);
//    ...........................................................................................................................   ENTER function declarations HERE (OPTIONAL - helps with compiling)


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-




//-=-=-=-=-=- Required Functions -=-=-=-=-=-=-=-
void setup() {

  if (DEBUG) {
    Serial.begin(19200);
  }
  SPI.begin();
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
  Timer3.initialize(2000); //sets to run every 2000usec
  Timer3.attachInterrupt(updateTouchMat);
  Serial1.begin(19200);
  Serial2.begin(31250);
  for (int tx = 0; tx < TSwidth; tx++) {
    for (int ty = 0; ty < TSheight; ty++) {
      touchmat[ty][tx] = 0;
      scaleddispmat[ty][tx] = 0;
    }
  }
  delay(100);
  pinMode(7, INPUT);
  digitalWrite(7, HIGH);

  delay(700);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  for (int i = 0; i < (TSwidth * TSheight); i++) {
    SPI.transfer(0xEF);
    SPI.transfer((byte)0x0);
    SPI.transfer((byte)0x0);
    SPI.transfer((byte)0x0);
  }
  //  touch.update();
  if (difydisp < difxdisp) {
    dispscaling = difxdisp;
  } else if (difydisp > difxdisp) {
    dispscaling = -1 * difydisp;
  } else {
    dispscaling = difxdisp;
  }
  if ((int)dispscaling == 0) {
    upscale = (int) (1 / dispscaling);
  } else {
    downscale = (int) dispscaling;
  }
  demo = 0;
  run = 0;
  attachInterrupt(digitalPinToInterrupt(demopin), dvs, CHANGE);
  attachInterrupt(digitalPinToInterrupt(runpin), rvs, CHANGE);
}

void loop() {
  request = 0;
  ledsOff();
  do {
    if (cs_4_6.capacitiveSensor(30) > btsco) {
      request = 1;
      digitalWrite(requestpin, request);
    }
    demo = digitalRead(demopin);
    run = digitalRead(runpin);
  } while (!demo & !run);
  if (run) {
    ledsOn();
    game();
  } else if (demo) {
    ledsOn();
    demofunction();
  }
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-



//-=-=-=-=-=-=- Drive Functions -=-=-=-=-=-=-=-
void noteOn(int pitch, int velocity, int instrument) {
  Serial2.write(0xB0);
  Serial2.write((byte)0x0);
  Serial2.write((byte)0x0);


  Serial2.write(0xC0); //Send "program change" (instrument change) message.
  Serial2.write(instrument); //Send the 8-bit number that corresponds to the desired instrument.
  Serial2.write(0x90);
  Serial2.write(pitch);
  Serial2.write(velocity);
}

void noteOff(int pitch, int velocity) {
  Serial2.write(0x80); //standard midi "note off" command
  Serial2.write(pitch);
  Serial2.write(velocity);
}

void noteDrums(int velocity, int instrument) {
  Serial2.write(0xB1);
  Serial2.write((byte)0x00);
  Serial2.write(0x78);

  Serial2.write(0xC1); //Send "program change" (instrument change) message.
  Serial2.write(instrument); //Send the 8-bit number that corresponds to the desired instrument.
  Serial2.write(0x90);
  Serial2.write(instrument);
  Serial2.write(velocity);
}

void dvs() {
  demo = digitalRead(demopin);
  if (demo) {
    ledsOn();
  } else {
    ledsOff();
  }
}

void rvs() {
  run = digitalRead(runpin);
  if (run) {
    ledsOn();
  } else {
    ledsOff();
  }
}

void ledsOn() {
  digitalWrite(redLED, HIGH);
  digitalWrite(greenLED, HIGH);
  digitalWrite(blueLED, HIGH);
}

void ledsOff() {
  digitalWrite(redLED, LOW);
  digitalWrite(greenLED, LOW);
  digitalWrite(blueLED, LOW);
}

void displayupdate() {
  bright = analogRead(brightpin) / 0x3FF;
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  if (difxdisp == 1) {
    for (dy = 0; dy < TSheight; dy++) {
      for (dx = 0; dy < TSwidth; dx++) {
        SPI.transfer((byte)(colrs[gameboard[dy][dx]][0]*bright));
        SPI.transfer((byte)(colrs[gameboard[dy][dx]][1]));
        SPI.transfer((byte)(colrs[gameboard[dy][dx]][2]));
        SPI.transfer((byte)(colrs[gameboard[dy][dx]][3]));
      }
    }
  } else if (difxdisp == 2) {
    for (dy = 0; dy < gameboardy; dy = 2 + dy) {
      for (dx = 0; dy < gameboardx; dx = 2 + dx) {
        scred = (colrs[gameboard[dy][dx]][3] + colrs[gameboard[dy + 1][dx]][3] + colrs[gameboard[dy][dx + 1]][3] + colrs[gameboard[dy + 1][dx + 1]][3]) / 4;
        scgreen = (colrs[gameboard[dy][dx]][2] + colrs[gameboard[dy + 1][dx]][2] + colrs[gameboard[dy][dx + 1]][2] + colrs[gameboard[dy + 1][dx + 1]][2]) / 4;
        scblue = (colrs[gameboard[dy][dx]][1] + colrs[gameboard[dy + 1][dx]][1] + colrs[gameboard[dy][dx + 1]][1] + colrs[gameboard[dy + 1][dx + 1]][1]) / 4;
        scbright = (colrs[gameboard[dy][dx]][0] + colrs[gameboard[dy + 1][dx]][0] + colrs[gameboard[dy][dx + 1]][0] + colrs[gameboard[dy + 1][dx + 1]][0]) / 4;
        SPI.transfer((byte)(scbright * bright));
        SPI.transfer((byte)scblue);
        SPI.transfer((byte)scgreen);
        SPI.transfer((byte)scred);
      }
    }
  } else if (difxdisp == 3) {
    for (dy = 1; dy < gameboardy; dy = 3 + dy) {
      for (dx = 1; dy < gameboardx; dx = 3 + dx) {
        scred = (colrs[gameboard[dy][dx]][3] + colrs[gameboard[dy + 1][dx]][3] + colrs[gameboard[dy][dx + 1]][3] + colrs[gameboard[dy + 1][dx + 1]][3] + colrs[gameboard[dy - 1][dx]][3] + colrs[gameboard[dy][dx - 1]][3] + colrs[gameboard[dy - 1][dx - 1]][3]) / 7;
        scgreen = (colrs[gameboard[dy][dx]][2] + colrs[gameboard[dy + 1][dx]][2] + colrs[gameboard[dy][dx + 1]][2] + colrs[gameboard[dy + 1][dx + 1]][2] + colrs[gameboard[dy - 1][dx]][2] + colrs[gameboard[dy][dx - 1]][2] + colrs[gameboard[dy - 1][dx - 1]][2]) / 7;
        scblue = (colrs[gameboard[dy][dx]][1] + colrs[gameboard[dy + 1][dx]][1] + colrs[gameboard[dy][dx + 1]][1] + colrs[gameboard[dy + 1][dx + 1]][1] + colrs[gameboard[dy - 1][dx]][1] + colrs[gameboard[dy][dx - 1]][1] + colrs[gameboard[dy - 1][dx - 1]][1]) / 7;
        scbright = (colrs[gameboard[dy][dx]][0] + colrs[gameboard[dy + 1][dx]][0] + colrs[gameboard[dy][dx + 1]][0] + colrs[gameboard[dy + 1][dx + 1]][0] + colrs[gameboard[dy - 1][dx]][0] + colrs[gameboard[dy][dx - 1]][0] + colrs[gameboard[dy - 1][dx - 1]][0]) / 7;
        SPI.transfer((byte)(scbright * bright));
        SPI.transfer((byte)scblue);
        SPI.transfer((byte)scgreen);
        SPI.transfer((byte)scred);
      }
    }
  } else {

  }
}

void updateTouchMat(void)
{
  //__________________________________  needs to be optimized!!!
  prevmil = millis();
  do {
    yloc = (uint8_t) Serial1.read();
  } while ((yloc == -1) | (prevmil == millis()));
  do {
    xloc = (uint8_t) Serial1.read();
  } while ((xloc == -1) | (prevmil == millis()));
  do {
    tvalue = (uint8_t) Serial1.read();
  } while ((tvalue == -1) | (prevmil == millis()));
  if (prevmil == millis()) {
    touchmat[yloc][xloc] = tvalue;
  }
}
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-


//    ...........................................................................................................................   ENTER Custom Functions HERE
//-=-=-=-=-=-=- Game Functions -=-=-=-=-=-=-=-
void game() {

}

void demofunction() {

}

