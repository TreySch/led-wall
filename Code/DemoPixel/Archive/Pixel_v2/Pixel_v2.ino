#include <SPI.h>
#include <CapacitiveSensor.h>

#define DEBUG 1
#define roff 70
#define goff 70
#define boff 70
#define cutoff 70

int red;
int green;
int blue;
int bright;

int i = 0;
int j = 0;

long total1 =  0;
long total2 =  0;
long total3 =  0;

long rtotal1 =  0;
long rtotal2 =  0;
long rtotal3 =  0;

long into1 = 0;
long into2 = 0;
long into3 = 0;

CapacitiveSensor   cs_4_2 = CapacitiveSensor(4, 2);       // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
CapacitiveSensor   cs_4_6 = CapacitiveSensor(4, 6);       // 10M resistor between pins 4 & 6, pin 6 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_4_8 = CapacitiveSensor(4, 8);       // 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil

void recal(void);


void setup() {

  //cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);     // turn off autocalibrate on channel 1 - just as an example

  if (DEBUG) {
    Serial.begin(9600);
  }
  SPI.begin();//SPISettings(8000000, MSBFIRST, SPI_MODE3));
  delay(100);
  pinMode(7, INPUT);
  digitalWrite(7, HIGH);
  red = 0;
  green = 0;
  blue = 0;

  delay(700);

  for (i = 0; i < 6; i++) {
    if (i % 2) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0xEF);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
    } else {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0xFF);
      SPI.transfer(0xEF);
      SPI.transfer(0xEF);
      SPI.transfer(0xEF);
    }

    total1 =  0;
    total2 =  0;
    total3 =  0;
    for (j = 0; j < 5; j++) {
      total1 = total1 + cs_4_2.capacitiveSensor(30);
      total2 = total2 + cs_4_6.capacitiveSensor(30);
      total3 = total3 + cs_4_8.capacitiveSensor(30);
    }
    total1 = total1 / 5;
    total2 = total2 / 5;
    total3 = total3 / 5;

    into1 = into1 + total1;
    into2 = into2 + total2;
    into3 = into3 + total3;
    delay(300);
  }
  j = 0;
  //into1 = into1 / 6;
  //into2 = into2 / 6;
  //into3 = into3 / 6;
}

void loop() {

  total1 =  0;
  total2 =  0;
  total3 =  0;
  for (i = 0; i < 5; i++) {
    total1 = total1 + cs_4_2.capacitiveSensor(30);
    total2 = total2 + cs_4_6.capacitiveSensor(30);
    total3 = total3 + cs_4_8.capacitiveSensor(30);
  }
  total1 = total1 / 5;
  total2 = total2 / 5;
  total3 = total3 / 5;

  if ((total1 - cutoff - into1 - roff) > 0) {
    red = (200 / 30) * sqrt(total1 - cutoff - into1 - roff);
    if (red > 0xFF) {
      red = 0xFF;
    }
  } else {
    red = (byte)0x0;
  }

  if ((total2 - cutoff - into2 - goff) > 0) {
    green = (200 / 30) * sqrt(total2 - cutoff - into2 - goff);
    if (green > 0xFF) {
      green = 0xFF;
    }
  } else {
    green = (byte)0x0;
  }

  if ((total3 - cutoff - into3 - boff) > 0) {
    blue = (200 / 30) * sqrt(total3 - cutoff - into3 - boff);
    if (blue > 0xFF) {
      blue = 0xFF;
    }
  } else {
    blue = (byte)0x0;
  }
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0xFF);
  SPI.transfer(blue);
  SPI.transfer(green);
  SPI.transfer(red);

  if (DEBUG) {
    Serial.print(into1);                  // print sensor output 1
    Serial.print("\t");
    Serial.print(into2);                  // print sensor output 2
    Serial.print("\t");
    Serial.println(into3);
    Serial.print(total1);                  // print sensor output 1
    Serial.print("\t");
    Serial.print(total2);                  // print sensor output 2
    Serial.print("\t");
    Serial.println(total3);
    delay(10);
  }
  recal();
}

void recal() {
  j++;
  if (total1 < (roff + into1)) {
    rtotal1 = rtotal1 + total1;
  } else {
    rtotal1 = rtotal1 + (rtotal1 / j);
  }
  if (total2 < (goff + into2)) {
    rtotal2 = rtotal2 + total2;
  } else {
    rtotal2 = rtotal2 + (rtotal2 / j);
  }
  if (total3 < (boff + into3)) {
    rtotal3 = rtotal3 + total3;
  } else {
    rtotal3 = rtotal3 + (rtotal3 / j);
  }
  if (j == 4) {
    j = 0;
    rtotal1 = rtotal1 / 5;
    rtotal2 = rtotal2 / 5;
    rtotal3 = rtotal3 / 5;

    into1 = rtotal1;
    into2 = rtotal2;
    into3 = rtotal3;
  }

}

