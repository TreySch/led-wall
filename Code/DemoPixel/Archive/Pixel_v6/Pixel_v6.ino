#include <SPI.h>
#include <CapacitiveSensor.h>

/*  NOTES:
    >>  Global variables use less mamory so try to use those inplace of local variables as often as possible
*/

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#define sampleres 30 //default 30
#define DEBUG 0
#define SPEEDTEST 0
#define MODE 0 //  1-color add, 2-nightlight, 3-color sub, 0-no preset
#define BRIPRE 0x00 //Brightness preset 0x02-0x1F. 0x00-no preset
#define roff 20
#define goff 20
#define boff 20 //30
#define cutoff 00 //0
#define linear 0
#define samples 2 //2
#define bleed 1 //1
#define bleeddiv 10 //10
#define sensitivity 4 //4
#define minamb 150 //100
#define nlspeed 100 //milliseconds between led update - 150 
#define nlseqrand 0 //night light sequence randomizer (0- false, 1- true)
#define nlseqnumb 10 //max number of color inputs
#define nlbritimslw 0 //0-realtime, 1-slows brightness when dimmed 
#define nlbritcutoff 1 //removes brightness adjust when below 0x07


#define powerpin 3
#define brightpin A1
#define mode1pin 10
#define mode2pin 11
#define mode3pin 12
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

int red;
int green;
int blue;
int bright;

int i = 0;
int j = 0;
int st = 0;

long total1 =  0;
long total2 =  0;
long total3 =  0;

long rtotal1 =  0;
long rtotal2 =  0;
long rtotal3 =  0;

long into1 = 0;
long into2 = 0;
long into3 = 0;

volatile int brightpd;
volatile int redpd;
volatile int greenpd;
volatile int bluepd;
int nightlightseq[nlseqnumb];
unsigned long nltim = 0;
unsigned long nltime = 0;
bool nlflag = 0; //used to trigger capturing custom light sequences
int nlseqinputnumb = 0;
int brightran = 0xE0; //temporary brightness
int brightnltemp = 0;
int redtemp = 0;
int greentemp = 0;
int bluetemp = 0;
int seqplace = 0;
bool randrq = 1;

CapacitiveSensor   cs_4_2 = CapacitiveSensor(4, 2);       // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
CapacitiveSensor   cs_4_6 = CapacitiveSensor(4, 6);       // 10M resistor between pins 4 & 6, pin 6 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_4_8 = CapacitiveSensor(4, 8);       // 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil

void recal(void);
void powerdown(void);

void nightlight(void);
void nlseqcapture(void);
void coloradd(void);
void colorsub(void);
void speedtestfunction(void);


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Setup -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void setup() {
  if (DEBUG) {
    Serial.begin(115200);
  }
  SPI.begin();//SPISettings(1000000, MSBFIRST, SPI_MODE0));
  SPI.beginTransaction(SPISettings(100000, MSBFIRST, SPI_MODE0));
  //  cs_4_2.set_CS_Timeout_Millis(100);
  //  cs_4_6.set_CS_Timeout_Millis(100);
  //  cs_4_8.set_CS_Timeout_Millis(100);
  delay(100);
  pinMode(powerpin, INPUT);
  pinMode(mode1pin, INPUT);
  pinMode(mode2pin, INPUT);
  pinMode(mode3pin, INPUT);
  digitalWrite(powerpin, HIGH);
  digitalWrite(mode1pin, HIGH);
  digitalWrite(mode2pin, HIGH);
  digitalWrite(mode3pin, HIGH);

  attachInterrupt(digitalPinToInterrupt(powerpin), powerdown, LOW);

  red = 0;
  green = 0;
  blue = 0;
  if (BRIPRE > 0) {
    bright =  BRIPRE;
  } else {
    bright =  2 + (int)((((float)0x1D / 0x3FF) * (float) analogRead(brightpin)));
  }

  delay(700);

  for (i = 0; i < 10; i++) {
    if (i % 2) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(bright + 0xE0);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
    } else {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(bright + 0xE0);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
    }

    total1 =  0;
    total2 =  0;
    total3 =  0;
    for (j = 0; j < 5; j++) {
      total1 = total1 + cs_4_2.capacitiveSensor(sampleres);
      total2 = total2 + cs_4_6.capacitiveSensor(sampleres);
      total3 = total3 + cs_4_8.capacitiveSensor(sampleres);
    }
    total1 = total1 / 5;
    total2 = total2 / 5;
    total3 = total3 / 5;

    into1 = into1 + total1;
    into2 = into2 + total2;
    into3 = into3 + total3;
    delay(50);
  }
  j = 0;
  into1 = into1 / 10;
  into2 = into2 / 10;
  into3 = into3 / 10;
  srand((unsigned int)(into1 + into2 + into3) + cs_4_2.capacitiveSensor(sampleres));
  if (BRIPRE > 0) {
    bright = BRIPRE;
  }
  nlflag = 0;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Loop -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void loop() {
  if (SPEEDTEST) {
    st = 0;
    speedtestfunction();
  } else if ((!digitalRead(mode1pin)) | (MODE == 1)) {
    coloradd();
  } else if ((!digitalRead(mode2pin)) | (MODE == 2)) {
    if (nlflag == 0) {
      if (DEBUG) {
        Serial.println("-=-=-=-=-=-=-=-=-=-=-=-=-=-=- TIMEOUT -=-=-=-=-=-=-=-=-=-=-=-=-=-=-");
      }
      nlflag = 1;
      nlseqcapture();
      nltime = millis();
    }
    nightlight();
  } else if ((!digitalRead(mode3pin)) | (MODE == 3)) {
    colorsub();
  } else {
    coloradd();
  }

  if ((digitalRead(mode2pin)) & nlflag) {
    nlflag = 0;
  }

  if (BRIPRE > 0) {
    bright =  BRIPRE;
  } else {
    bright =  2 + (int)((((float)0x1D / 0x3FF) * (float) analogRead(brightpin)));
  }

  nltim = (0x100 - (byte)bright) / 8;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Color Addition -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void coloradd() {
  total1 =  0;
  total2 =  0;
  total3 =  0;
  for (i = 0; i < samples; i++) {
    total1 = total1 + cs_4_2.capacitiveSensor(sampleres);
    total2 = total2 + cs_4_6.capacitiveSensor(sampleres);
    total3 = total3 + cs_4_8.capacitiveSensor(sampleres);
  }
  total1 = total1 / samples;
  total2 = total2 / samples;
  total3 = total3 / samples;

  if (!linear) {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (255 / 30) * sqrt(total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 30) * sqrt(total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 30) * sqrt(total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  } else {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (700 / 255) * (total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 74) * (total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 255) * (total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  }

  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(bright + 0xE0);
  SPI.transfer(blue);
  SPI.transfer(green);
  SPI.transfer(red);

  if (DEBUG) {
    if (((DEBUG) | (total1 - cutoff - into1 - roff) > 0) | ((total2 - cutoff - into2 - goff) > 0) | ((total3 - cutoff - into3 - boff) > 0)) {
      Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
      Serial.print(cutoff + into1 + roff);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(cutoff + into2 + goff);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3 + cutoff + into3 + boff);
      Serial.print(total1 - cutoff - into1 - roff);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(total2 - cutoff - into2 - goff);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3 - cutoff - into3 - boff);
      Serial.print(total1);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(total2);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3);
      if ((total1 - cutoff - into1 - roff) > 0) {
        Serial.print("R");
      } else {
        Serial.print(" ");
      }
      Serial.print("\t");
      if ((total2 - cutoff - into2 - goff) > 0) {
        Serial.print("G");
      } else {
        Serial.print(" ");
      }
      Serial.print("\t");
      if ((total3 - cutoff - into3 - boff) > 0) {
        Serial.println("B");
      } else {
        Serial.println(" ");
      }
      Serial.print(red);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(green);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(blue);
      Serial.print(analogRead(brightpin));
      Serial.print("\t");
      Serial.println(bright);
    }
  }
  recal();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Color Subtract -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void colorsub() {
  total1 =  0;
  total2 =  0;
  total3 =  0;
  for (i = 0; i < samples; i++) {
    total1 = total1 + cs_4_2.capacitiveSensor(sampleres);
    total2 = total2 + cs_4_6.capacitiveSensor(sampleres);
    total3 = total3 + cs_4_8.capacitiveSensor(sampleres);
  }
  total1 = total1 / samples;
  total2 = total2 / samples;
  total3 = total3 / samples;

  if (!linear) {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (255 / 30) * sqrt(total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 30) * sqrt(total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 30) * sqrt(total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  } else {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (700 / 255) * (total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 74) * (total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 255) * (total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  }



  if (red > 0) {
    if (green > 0) {
      if (blue > 0) {
        //
        bright = ((bright) / 3);
        red = 0xFF - ((red + green + blue) / 3);
        green = red;
        blue = red;
      } else {
        red = red;
        green = (green / 2) - (red / 4);
        bright = ((bright) / 2);
        //bright = bright - ((0x1F / 0xFF) * (red + green) / 2);
      }
    } else if (blue > 0) {
      bright = ((bright) / 2);
      //bright = bright - ((0x1F / 0xFF) * (red + blue) / 2);
    } else {
      bright = bright; //
    }
  } else if (green > 0) {
    if (blue > 0) {
      bright = ((bright) / 2);
      //bright = bright - ((0x1F / 0xFF) * (green + blue) / 2);
      green = (green + blue) / 2;
      red = 0;
      blue = 0;
    } else {
      bright = bright; //
      red = green;//............... color
    }
  } else {
    bright = bright; //
  }

  if (bright < 0) {
    bright = 0;
  }

  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(bright + 0xE0);
  SPI.transfer(blue);
  SPI.transfer(green);
  SPI.transfer(red);

  if (DEBUG) {
    if (((total1 - cutoff - into1 - roff) > 0) | ((total2 - cutoff - into2 - goff) > 0) | ((total3 - cutoff - into3 - boff) > 0)) {
      Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
      Serial.print(cutoff + into1 + roff);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(cutoff + into2 + goff);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3 + cutoff + into3 + boff);
      Serial.print(red);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(green);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(blue);
      Serial.print(total1);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(total2);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3);
      if ((total1 - cutoff - into1 - roff) > 0) {
        Serial.print("R");
      } else {
        Serial.print(" ");
      }
      Serial.print("\t");
      if ((total2 - cutoff - into2 - goff) > 0) {
        Serial.print("Y");
      } else {
        Serial.print(" ");
      }
      Serial.print("\t");
      if ((total3 - cutoff - into3 - boff) > 0) {
        Serial.println("B");
      } else {
        Serial.println(" ");
      }
      Serial.print("\t");
      Serial.println(bright - 0xDF);
    }
  }
  recal();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Night Light -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void nightlight() {
  //int brightran = 0; //temporary brightness
  //int brightnltemp = 0;
  //int redtemp = 0;
  //int greentemp = 0;
  //int bluetemp = 0;
  //int seqplace = 0;
  //bool randrq = 1;
  //-=-=-=-=-=-=-=-=-=-=-=-=-=- SETUP
  if (randrq) {
    brightran = 1 + (rand() % (bright));
    randrq = 0;
    seqplace = rand() % (nlseqinputnumb);
    red = 0;
    green = 0;
    blue = 0;
    if ((nightlightseq[seqplace] == 1) | (nightlightseq[seqplace] == 5) | (nightlightseq[seqplace] == 6) | (nightlightseq[seqplace] == 7)) {
      red = rand() % 256;
    }
    if ((nightlightseq[seqplace] == 2) | (nightlightseq[seqplace] == 4) | (nightlightseq[seqplace] == 6) | (nightlightseq[seqplace] == 7)) {
      if ((nightlightseq[seqplace] == 6) | (nightlightseq[seqplace] == 7)) {
        blue = red;
      } else {
        blue = (rand() % 256);
      }
    }
    if ((nightlightseq[seqplace] == 3) | (nightlightseq[seqplace] == 4) | (nightlightseq[seqplace] == 5) | (nightlightseq[seqplace] == 7)) {
      if ((nightlightseq[seqplace] == 5) | (nightlightseq[seqplace] == 7)) {
        green = red;
      } else if (nightlightseq[seqplace] == 4) {
        green = blue;
      } else {
        green = (rand() % 256);
      }
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-
  } else {
    nltime++;
    if ((nltime > nltim) | (!nlbritimslw)) {
      nltime = 1;
      if ((!nlbritcutoff) | (bright > 0x07)) {
        if (brightnltemp > brightran) {
          brightnltemp--;
        } else if (brightnltemp < brightran) {
          brightnltemp++;
        } else {
          brightnltemp = brightnltemp;
          if (BRIPRE > 0) {
            brightran =  1 + (rand() % (BRIPRE));
          } else {
            brightran =  1 + (rand() % (bright));
          }
        }
      } else {
        brightnltemp = bright;
      }
    }

    if (redtemp > red) {
      redtemp--;
    } else if (redtemp < red) {
      redtemp++;
    } else {
      redtemp = redtemp;
    }
    if (bluetemp > blue) {
      bluetemp--;
    } else if (bluetemp < blue) {
      bluetemp++;
    } else {
      bluetemp = bluetemp;
    }
    if (greentemp > green) {
      greentemp--;
    } else if (greentemp < green) {
      greentemp++;
    } else {
      greentemp = greentemp;
    }
    if ((greentemp == green) & (bluetemp == blue) & (redtemp == red)) {
      randrq = 1;
    }
  }
  if (DEBUG) {
    Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
    if (seqplace == 0) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[0]);
    Serial.print("\t");
    if (seqplace == 1) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[1]);
    Serial.print("\t");
    if (seqplace == 2) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[2]);
    Serial.print("\t");
    if (seqplace == 3) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[3]);
    Serial.print("\t");
    if (seqplace == 4) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[4]);
    Serial.print("\t");
    if (seqplace == 5) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[5]);
    Serial.print("\t");
    if (seqplace == 6) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[6]);
    Serial.print("\t");
    if (seqplace == 7) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[7]);
    Serial.print("\t");
    if (seqplace == 8) {
      Serial.print("|");
    }
    Serial.print(nightlightseq[8]);
    Serial.print("\t");
    if (seqplace == 9) {
      Serial.print("|");
    }
    Serial.println(nightlightseq[9]);
    Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
    Serial.print(brightran);                  // print sensor output 1
    Serial.print("\t");
    Serial.print(red);                  // print sensor output 2
    Serial.print("\t");
    Serial.print(green);
    Serial.print("\t");
    Serial.println(blue);
    Serial.print(brightnltemp);                  // print sensor output 1
    Serial.print("\t");
    Serial.print(redtemp);                  // print sensor output 2
    Serial.print("\t");
    Serial.print(greentemp);
    Serial.print("\t");
    Serial.println(bluetemp);
    Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
    Serial.print(nltim);
    Serial.print("\t");
    Serial.println(nltime);
  }
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer((byte) brightnltemp + 0xE0);
  SPI.transfer((byte) bluetemp);
  SPI.transfer((unsigned byte) greentemp);
  SPI.transfer((byte) redtemp);
  delay(nlspeed);
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=- Sequence Capture -=-=-=-=-=-=-=-=-=-=-=-=-=-
void nlseqcapture() {
  //-=-=-=-=-=-=-=-=-=-=- Initialization
  int brighttemp = 0;
  unsigned long timeout = 0;
  int u = 0;

  for (u = 0; u < nlseqnumb; u++) {
    nightlightseq[u] = 0;
  }
  if (DEBUG) {
    Serial.print(bright);
  }
  //-=-=-=-=-=-=-=-=-=-=- Initial Animation
  while (brighttemp < bright) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer((brighttemp + 0xE0));
    SPI.transfer((byte)0x00);
    SPI.transfer((byte)0x00);
    SPI.transfer(0xEF);
    delay(50);
    brighttemp++;
  }
  while (brighttemp > 0) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer((brighttemp + 0xE0));
    SPI.transfer((byte)0x00);
    SPI.transfer(0xEF);
    SPI.transfer((byte)0x00);
    delay(30);
    brighttemp--;
  }
  while (brighttemp < bright) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer((brighttemp + 0xE0));
    SPI.transfer(0xEF);
    SPI.transfer((byte)0x00);
    SPI.transfer((byte)0x00);
    delay(20);
    brighttemp++;
  }
  //-=-=-=-=-=-=-=-=-=-=- Initialization
  for (u = 0; u < nlseqnumb; u++) {
    timeout = millis();
    do {
      coloradd();
      if ((red == 0xFF) | (blue == 0xFF) | (green == 0xFF)) {
        for (int a = 0; a < 30; a++) {
          coloradd();
        }
      }
    } while ((millis() < (timeout + 1400)) & !((red == 0xFF) | (blue == 0xFF) | (green == 0xFF)));
    if (red == 0xFF) {
      if (blue == 0xFF) {
        if (green == 0xFF) {
          nightlightseq[u] = 7; //white
        } else {
          nightlightseq[u] = 6;//magenta
        }
      } else if (green == 0xFF) {
        nightlightseq[u] = 5; //yellow
      } else {
        nightlightseq[u] = 1;  //red
      }
    } else if (blue == 0xFF) {
      if (green == 0xFF) {
        nightlightseq[u] = 4; //cyan
      } else {
        nightlightseq[u] = 2; //blue
      }
    } else if (green == 0xFF) {
      nightlightseq[u] = 3; //green
    } else {
      nightlightseq[u] = 0; //black/error
      u = nlseqnumb;
    }
    brighttemp = 0;
    while (brighttemp < bright) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brighttemp + 0xE0);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      delay(30);
      brighttemp++;
    }
    brighttemp = 0;
    while (brighttemp < bright) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brighttemp + 0xE0);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      delay(30);
      brighttemp++;
    }
    if (nightlightseq[u] == 0) {
      u = nlseqnumb;
    }
  }

  nlseqinputnumb = 0;
  timeout = 0; //now being used as a NULL check for sequence total
  for (u = 0; u < nlseqnumb; u++) {
    timeout = timeout + nightlightseq[u];
    if (nightlightseq[u] > 0) {
      nlseqinputnumb++;
    }
  }
  if (timeout == 0) {
    for (u = 1; u < 8; u++) {
      nightlightseq[u - 1] = u;
    }
    nlseqinputnumb = 7;
  }
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Recalibrate -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void recal() {
  j++;
  if (total1 < (roff + into1)) {
    rtotal1 = rtotal1 + total1;
  } else {
    rtotal1 = rtotal1 + (rtotal1 / j);
  }
  if (total2 < (goff + into2)) {
    rtotal2 = rtotal2 + total2;
  } else {
    rtotal2 = rtotal2 + (rtotal2 / j);
  }
  if (total3 < (boff + into3)) {
    rtotal3 = rtotal3 + total3;
  } else {
    rtotal3 = rtotal3 + (rtotal3 / j);
  }
  if (j == 4) {
    j = 0;
    rtotal1 = rtotal1 / 5;
    rtotal2 = rtotal2 / 5;
    rtotal3 = rtotal3 / 5;

    into1 = rtotal1 + (bleed * total2 / bleeddiv) + (bleed * total3 / bleeddiv);
    if (into1 < minamb) {
      into1 = minamb;
    }
    into2 = rtotal2 + (bleed * total1 / bleeddiv) + (bleed * total3 / bleeddiv);
    if (into2 < minamb) {
      into2 = minamb;
    }
    into3 = rtotal3 + (bleed * total2 / bleeddiv) + (bleed * total1 / bleeddiv);
    if (into3 < minamb) {
      into3 = minamb;
    }
  }
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Speed Test -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void speedtestfunction() {
  st++;
  if (st == 4) {
    st = 1;
  }
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(bright + 0xE0);
  if (st == 1) {
    SPI.transfer(0xFF);
  } else {
    SPI.transfer(0x00);
  }
  if (st == 2) {
    SPI.transfer(0xFF);
  } else {
    SPI.transfer(0x00);
  }
  if (st == 3) {
    SPI.transfer(0xFF);
  } else {
    SPI.transfer(0x00);
  }
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Power Down -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void powerdown() {
  brightpd = bright;
  redpd = red;
  bluepd = blue;
  greenpd = green;
  if (DEBUG) {
    Serial.println("Power Down");
  }
  if ((!digitalRead(mode2pin)) | (MODE == 2)) {
    brightpd = brightnltemp;
    redpd = redtemp;
    bluepd = bluetemp;
    greenpd = greentemp;
  }
  do {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(brightpd + 0xE0);
    SPI.transfer(bluepd);
    SPI.transfer(greenpd);
    SPI.transfer(redpd);
    if (DEBUG) {
      Serial.print("down -");
      Serial.print("\t");
      Serial.println(brightpd);
    }
    delayMicroseconds(10000000);
  } while (brightpd-- > 0);
  do {
    delayMicroseconds(300000);
  } while (!digitalRead(powerpin));
  delayMicroseconds(10000000);
  into1 = cs_4_2.capacitiveSensor(sampleres);
  into2 = cs_4_6.capacitiveSensor(sampleres);
  into3 = cs_4_8.capacitiveSensor(sampleres);
  brightpd++;
  if ((!digitalRead(mode2pin)) | (MODE == 2)) {
    do {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brightpd + 0xE0);
      SPI.transfer(bluepd);
      SPI.transfer(greenpd);
      SPI.transfer(redpd);
      if (DEBUG) {
        Serial.print("up -");
        Serial.print("\t");
        Serial.println(brightpd);
      }
      delayMicroseconds(10000000);
    } while (brightpd++ < brightnltemp);
  } else {
    do {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brightpd + 0xE0);
      SPI.transfer(bluepd);
      SPI.transfer(greenpd);
      SPI.transfer(redpd);
      if (DEBUG) {
        Serial.print("up -");
        Serial.print("\t");
        Serial.println(brightpd);
      }
      delayMicroseconds(10000000);
    } while (brightpd++ < bright);
  }
}
