#include <SPI.h>
#include <CapacitiveSensor.h>

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#define sampleres 3 //default 30
#define DEBUG 1
#define SPEEDTEST 0
#define roff 30
#define goff 30
#define boff 30 //30
#define cutoff 2000 //0
#define linear 0
#define samples 2 //2
#define bleed 1 //1
#define bleeddiv 20 //10
#define sensitivity 1 //4
#define minamb 30000 //100
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

int red;
int green;
int blue;
int bright;

int i = 0;
int j = 0;
int st = 0;

long total1 =  0;
long total2 =  0;
long total3 =  0;

long rtotal1 =  0;
long rtotal2 =  0;
long rtotal3 =  0;

long into1 = 0;
long into2 = 0;
long into3 = 0;

CapacitiveSensor   cs_4_2 = CapacitiveSensor(4, 2);       // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
CapacitiveSensor   cs_4_6 = CapacitiveSensor(4, 6);       // 10M resistor between pins 4 & 6, pin 6 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_4_8 = CapacitiveSensor(4, 8);       // 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil

void recal(void);


void setup() {

  cs_4_2.set_CS_AutocaL_Millis(0x0000FFFF);     // turn off autocalibrate on channel 1 - just as an example

  if (DEBUG) {
    Serial.begin(115200);
  }
  SPI.begin();//SPISettings(8000000, MSBFIRST, SPI_MODE0));
  SPI.beginTransaction(SPISettings(1000000, MSBFIRST, SPI_MODE3));
  delay(100);
  pinMode(7, INPUT);
  digitalWrite(7, HIGH);
  red = 0;
  green = 0;
  blue = 0;

  delay(700);

  for (i = 0; i < 10; i++) {
    if (i % 2) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0xEF);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
    } else {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
    }

    total1 =  0;
    total2 =  0;
    total3 =  0;
    for (j = 0; j < 5; j++) {
      total1 = total1 + cs_4_2.capacitiveSensor(sampleres);
      total2 = total2 + cs_4_6.capacitiveSensor(sampleres);
      total3 = total3 + cs_4_8.capacitiveSensor(sampleres);
    }
    total1 = total1 / 5;
    total2 = total2 / 5;
    total3 = total3 / 5;

    into1 = into1 + total1;
    into2 = into2 + total2;
    into3 = into3 + total3;
    delay(50);
  }
  j = 0;
  into1 = into1 / 10;
  into2 = into2 / 10;
  into3 = into3 / 10;
}

void loop() {

  total1 =  0;
  total2 =  0;
  total3 =  0;
  for (i = 0; i < samples; i++) {
    total1 = total1 + cs_4_2.capacitiveSensor(sampleres);
    total2 = total2 + cs_4_6.capacitiveSensor(sampleres);
    total3 = total3 + cs_4_8.capacitiveSensor(sampleres);
  }
  total1 = total1 / samples;
  total2 = total2 / samples;
  total3 = total3 / samples;

  if (!linear) {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (255 / 30) * sqrt(total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 30) * sqrt(total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 30) * sqrt(total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  } else {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (700 / 255) * (total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 74) * (total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 255) * (total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  }


  if (!SPEEDTEST) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0xFF);
    SPI.transfer(blue);
    SPI.transfer(green);
    SPI.transfer(red);
  } else {
    st++;
    if (st == 4) {
      st = 1;
    }
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0xFF);
    if (st == 1) {
      SPI.transfer(0xFF);
    } else {
      SPI.transfer(0x00);
    }
    if (st == 2) {
      SPI.transfer(0xFF);
    } else {
      SPI.transfer(0x00);
    }
    if (st == 3) {
      SPI.transfer(0xFF);
    } else {
      SPI.transfer(0x00);
    }
  }

  if (DEBUG) {
    if (((total1 - cutoff - into1 - roff) > 0) | ((total2 - cutoff - into2 - goff) > 0) | ((total3 - cutoff - into3 - boff) > 0)) {
      Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
      Serial.print(cutoff + into1 + roff);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(cutoff + into2 + goff);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3 + cutoff + into3 + boff);
      Serial.print(total1 - cutoff - into1 - roff);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(total2 - cutoff - into2 - goff);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3 - cutoff - into3 - boff);
      Serial.print(total1);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(total2);                  // print sensor output 2
      Serial.print("\t");
      Serial.println(total3);
      if((total1 - cutoff - into1 - roff) > 0){
        Serial.print("R");
      }else{
        Serial.print(" ");
      }
      Serial.print("\t");
      if((total2 - cutoff - into2 - goff) > 0){
        Serial.print("G");
      }else{
        Serial.print(" ");
      }
      Serial.print("\t");
      if((total3 - cutoff - into3 - boff) > 0){
        Serial.println("B");
      }else{
        Serial.println(" ");
      }
      //delay(10);
    }
  }
  recal();
}

void recal() {
  j++;
  if (total1 < (roff + into1)) {
    rtotal1 = rtotal1 + total1;
  } else {
    rtotal1 = rtotal1 + (rtotal1 / j);
  }
  if (total2 < (goff + into2)) {
    rtotal2 = rtotal2 + total2;
  } else {
    rtotal2 = rtotal2 + (rtotal2 / j);
  }
  if (total3 < (boff + into3)) {
    rtotal3 = rtotal3 + total3;
  } else {
    rtotal3 = rtotal3 + (rtotal3 / j);
  }
  if (j == 4) {
    j = 0;
    rtotal1 = rtotal1 / 5;
    rtotal2 = rtotal2 / 5;
    rtotal3 = rtotal3 / 5;

    into1 = rtotal1 + (bleed * total2 / bleeddiv) + (bleed * total3 / bleeddiv);
    if (into1 < minamb) {
      into1 = minamb;
    }
    into2 = rtotal2 + (bleed * total1 / bleeddiv) + (bleed * total3 / bleeddiv);
    if (into2 < minamb) {
      into2 = minamb;
    }
    into3 = rtotal3 + (bleed * total2 / bleeddiv) + (bleed * total1 / bleeddiv);
    if (into3 < minamb) {
      into3 = minamb;
    }
  }

}

