#include <SPI.h>
#include <CapacitiveSensor.h>

/*  NOTES:
    >>  Global variables use less mamory so try to use those inplace of local variables as often as possible
    >>  NEXT ITERATION: fix cap dip
    >>  NEXT ITERATION: finalize input to LED formulas
    >>  NEXT ITERATION: finalize color mixing function
    >>  ITERATION GOAL: Optimize interruptible demo function
*/

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
#define sampleres 20 //default 30
#define samples 1 //2
#define AVG 1 //0-average of # samples, 1-running average of # samples
#define rsamples 7 //number of averaged samples

#define DEBUG 1 //prints info to Serial port
#define VISUALIZER 1 //used to better visualize unknown cap dip over serial monitor
#define DEBUGALWAYSUPDATE 1 //0-Prints only when LED is on, 1-prints values all the time

#define SPEEDTEST 0 //

#define MODE 0 //  1-color add, 2-demo, 3-color sub, 0-no preset
int prevMODE; //for demo to return to
#define BRIPRE 0x00 //Brightness preset 0x02-0x1F. 0x00-no preset

#define roff 0
#define goff 0
#define boff 0 //30
#define cutoff 100 //0
#define linear 1
int bleed = 0; //1
#define bleeddiv 150 //10
#define sensitivity 1 //4
int minamb1 = 150; //100
int minamb2 = 150; //100
int minamb3 = 150; //100


#define nlspeed 50 //milliseconds between led update - 150 
#define nlseqrand 0 //night light sequence randomizer (0- false, 1- true)
#define nlseqnumb 10 //max number of color inputs
#define nlbritimslw 0 //0-realtime, 1-slows brightness when dimmed 
#define nlbritcutoff 1 //removes brightness adjust when below 0x07

#define recalsamp 5 //number of samples used in recalibration

#define CAPDIPCORRECT 0 //throws out garbage readings
#define demotimetrigger 500 //
bool demoflag;
#define demofunc 1 //has a demo function that runs an LED light show

//PINS
#define powerpin 3
#define brightpin A1
#define mode1pin 10
#define mode2pin 11
#define mode3pin 12
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

int capdipcount = -40;

int red;
int green;
int blue;
int bright;

int i = 0;
int j = 0;
int st = 0;

int u = 0; //used for averaging

long total1 =  0;
long total2 =  0;
long total3 =  0;

long dbtotal1 =  0;
long dbtotal2 =  0;
long dbtotal3 =  0;

long rtotal1 =  0;
long rtotal2 =  0;
long rtotal3 =  0;

long into1 = 0;
long into2 = 0;
long into3 = 0;

unsigned long demotimer = 0;

int rsamplepos = 0;

volatile int brightpd;
volatile int redpd;
volatile int greenpd;
volatile int bluepd;
int demoseq[7] = {1, 2, 3, 4, 5, 6, 7};
long recordings[3][rsamples];
unsigned long nltim = 0;
unsigned long nltime = 0;
bool nlflag = 0; //used to trigger capturing custom light sequences
int nlseqinputnumb = 0;
int brightran = 0xE0; //temporary brightness
int brightnltemp = 0;
int redtemp = 0;
int greentemp = 0;
int bluetemp = 0;
int seqplace = 0;
bool randrq = 1;

CapacitiveSensor   cs_4_2 = CapacitiveSensor(4, 2);       // 10M resistor between pins 4 & 2, pin 2 is sensor pin, add a wire and or foil if desired
CapacitiveSensor   cs_4_6 = CapacitiveSensor(4, 6);       // 10M resistor between pins 4 & 6, pin 6 is sensor pin, add a wire and or foil
CapacitiveSensor   cs_4_8 = CapacitiveSensor(4, 8);       // 10M resistor between pins 4 & 8, pin 8 is sensor pin, add a wire and or foil

void recal(void);
void powerdown(void);

void demo(void);
void nlseqcapture(void);
void coloradd(void);
void colorsub(void);
void speedtestfunction(void);


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Setup -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void setup() {
  if (DEBUG) {
    Serial.begin(115200);
  }
  cs_4_2.set_CS_AutocaL_Millis(0xFFFFFFFF);
  cs_4_6.set_CS_AutocaL_Millis(0xFFFFFFFF);
  cs_4_8.set_CS_AutocaL_Millis(0xFFFFFFFF);
  SPI.begin();//SPISettings(1000000, MSBFIRST, SPI_MODE0));
  SPI.beginTransaction(SPISettings(100000, MSBFIRST, SPI_MODE0));
  //  cs_4_2.set_CS_Timeout_Millis(100);
  //  cs_4_6.set_CS_Timeout_Millis(100);
  //  cs_4_8.set_CS_Timeout_Millis(100);
  delay(100);
  pinMode(powerpin, INPUT);
  pinMode(mode1pin, INPUT);
  pinMode(mode2pin, INPUT);
  pinMode(mode3pin, INPUT);
  digitalWrite(powerpin, HIGH);
  digitalWrite(mode1pin, HIGH);
  digitalWrite(mode2pin, HIGH);
  digitalWrite(mode3pin, HIGH);

  attachInterrupt(digitalPinToInterrupt(powerpin), powerdown, LOW);

  red = 0;
  green = 0;
  blue = 0;
  if (BRIPRE > 0) {
    bright =  BRIPRE;
  } else {
    bright =  2 + (int)((((float)0x1D / 0x3FF) * (float) analogRead(brightpin)));
  }

  delay(700);

  for (i = 0; i < 10; i++) {
    if (i % 2) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(bright + 0xE0);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
      SPI.transfer((byte)0x0);
    } else {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(bright + 0xE0);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
      SPI.transfer(0xFF);
    }

    total1 =  0;
    total2 =  0;
    total3 =  0;
    for (j = 0; j < 5; j++) {
      total1 = total1 + cs_4_2.capacitiveSensorRaw(sampleres);
      total2 = total2 + cs_4_6.capacitiveSensorRaw(sampleres);
      total3 = total3 + cs_4_8.capacitiveSensorRaw(sampleres);
    }
    total1 = total1 / 5;
    total2 = total2 / 5;
    total3 = total3 / 5;

    into1 = into1 + total1;
    into2 = into2 + total2;
    into3 = into3 + total3;
    delay(50);
  }
  for (i = 0; i < 3; i++) {
    for (j = 0; j < rsamples; j++) {
      recordings[i][j] = 0;
    }
  }
  j = 0;
  into1 = into1 / 10;
  into2 = into2 / 10;
  into3 = into3 / 10;
  minamb1 = into1;
  minamb2 = into2;
  minamb3 = into3;
  if (DEBUG) {
    Serial.print("Minimum Ambient = ");
    Serial.println(minamb1, DEC);
  }
  srand((unsigned int)(into1 + into2 + into3) + cs_4_2.capacitiveSensor(sampleres));
  if (BRIPRE > 0) {
    bright = BRIPRE;
  }
  nlflag = 0;
  demoflag = 0;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Loop -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void loop() {
  if (SPEEDTEST) {
    st = 0;
    speedtestfunction();
  } else if (((!digitalRead(mode1pin)) | (MODE == 1) | ((prevMODE == 1) & (digitalRead(mode3pin) & (digitalRead(mode2pin))))) & !((demotimer > demotimetrigger) | demoflag)) {
  coloradd();
    prevMODE = 1;
  } else if ((!digitalRead(mode2pin)) | (MODE == 2) | (demotimer > demotimetrigger) | demoflag) {
  demoflag = 1;
  demo();
    if ((cs_4_2.capacitiveSensorRaw(sampleres) + cs_4_6.capacitiveSensorRaw(sampleres) + cs_4_8.capacitiveSensorRaw(sampleres) - (2 * cutoff) - (minamb1 + minamb2 + minamb3)) > 0) {
      demoflag = 0;
      demotimer = 0;
    }
  } else if (((!digitalRead(mode3pin)) | (MODE == 3) | ((prevMODE == 3) & (digitalRead(mode1pin) & (digitalRead(mode2pin))))) & !((demotimer > demotimetrigger) | demoflag)) {
  colorsub();
    prevMODE = 3;
  } else {
    coloradd();
  }
  if ((red == 0) & (green == 0) & (blue == 0) & !demoflag & demofunc) {
  demotimer++;
  if (DEBUG) {
      Serial.println(demotimer);
    }
  } else if (demotimer) {
  demotimer = 0;
}
if ((digitalRead(mode2pin)) & nlflag) {
  nlflag = 0;
}

if (BRIPRE > 0) {
  bright =  BRIPRE;
} else {
  bright =  2 + (int)((((float)0x1D / 0x3FF) * (float) analogRead(brightpin)));
  }

  nltim = (0x100 - (byte)bright) / 8;
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Color Addition -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void coloradd() {
  if (!AVG) {
    total1 =  0;
    total2 =  0;
    total3 =  0;
    for (i = 0; i < samples; i++) {
      total1 = total1 + cs_4_2.capacitiveSensorRaw(sampleres);
      total2 = total2 + cs_4_6.capacitiveSensorRaw(sampleres);
      total3 = total3 + cs_4_8.capacitiveSensorRaw(sampleres);
    }
    total1 = total1 / samples;
    total2 = total2 / samples;
    total3 = total3 / samples;
  } else {
    if (rsamplepos == rsamples) {
      rsamplepos = 0;
    }
    if (CAPDIPCORRECT) {
      if (capdipcount++ > 85) {
        capdipcount = 0;
        for (int r = 0; r < 15; r++) {
          total1 = cs_4_2.capacitiveSensorRaw(sampleres);
          total2 = cs_4_6.capacitiveSensorRaw(sampleres);
          total3 = cs_4_8.capacitiveSensorRaw(sampleres);
        }
      }
    }
    total1 = cs_4_2.capacitiveSensorRaw(sampleres);
    total2 = cs_4_6.capacitiveSensorRaw(sampleres);
    total3 = cs_4_8.capacitiveSensorRaw(sampleres);
    recordings[0][rsamplepos] = total1;
    recordings[1][rsamplepos] = total2;
    recordings[2][rsamplepos] = total3;
    total1 = 0;
    total2 = 0;
    total3 = 0;
    for (u = 0; u < rsamples; u++) {
      total1 = total1 + recordings[0][u];
    }
    for (u = 0; u < rsamples; u++) {
      total2 = total2 + recordings[1][u];
    }
    for (u = 0; u < rsamples; u++) {
      total3 = total3 + recordings[2][u];
    }
    total1 = total1 / rsamples;
    total2 = total2 / rsamples;
    total3 = total3 / rsamples;
    rsamplepos++;
  }

  if (!linear) {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (255 / 30) * sqrt(total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 30) * sqrt(total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 30) * sqrt(total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  } else {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (700 / 255) * (total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (700 / 255) * (total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (700 / 255) * (total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  }

  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(bright + 0xE0);
  SPI.transfer(blue);
  SPI.transfer(green);
  SPI.transfer(red);

  if (DEBUG) {
    if (!VISUALIZER) {
      if (((DEBUGALWAYSUPDATE) | (total1 - cutoff - into1 - roff) > 0) | ((total2 - cutoff - into2 - goff) > 0) | ((total3 - cutoff - into3 - boff) > 0)) {
        Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
        Serial.print(cutoff + into1 + roff);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(cutoff + into2 + goff);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(cutoff + into3 + boff);
        Serial.print(total1 - cutoff - into1 - roff);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(total2 - cutoff - into2 - goff);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(total3 - cutoff - into3 - boff);
        Serial.print(total1);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(total2);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(total3);
        if ((total1 - cutoff - into1 - roff) > 0) {
          Serial.print("R");
        } else {
          Serial.print(" ");
        }
        Serial.print("\t");
        if ((total2 - cutoff - into2 - goff) > 0) {
          Serial.print("G");
        } else {
          Serial.print(" ");
        }
        Serial.print("\t");
        if ((total3 - cutoff - into3 - boff) > 0) {
          Serial.println("B");
        } else {
          Serial.println(" ");
        }
        Serial.print(red);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(green);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(blue);
        Serial.print(analogRead(brightpin));
        Serial.print("\t");
        Serial.println(bright);
      }
    } else {
      int dbcdtemp1 = (1 * (total1 - cutoff - into1 - roff)) + 200; //10
      int dbcdtemp2 = (1 * (total2 - cutoff - into2 - goff)) + 200; //10
      int dbcdtemp3 = (1 * (total3 - cutoff - into3 - boff)) + 200; //10
      //int dbcdtemp = total1 - minamb1 + cutoff; //minamb and cutoff are both constants
      while ((dbcdtemp1 > -10) | (dbcdtemp2 > -10) | (dbcdtemp3 > -10)) {
        dbcdtemp1 = dbcdtemp1 - 10;
        dbcdtemp2 = dbcdtemp2 - 10;
        dbcdtemp3 = dbcdtemp3 - 10;
        if ((dbcdtemp1 > -10) & (dbcdtemp1 < 10)) {
          Serial.print("R");
        } else if ((dbcdtemp2 > -10) & (dbcdtemp2 < 10)) {
          Serial.print("G");
        } else if ((dbcdtemp3 > -10) & (dbcdtemp3 < 10)) {
          Serial.print("B");
        } else {
          Serial.print(" ");
        }
      }
      Serial.println(" ");
    }
  }
  recal();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Color Subtract -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void colorsub() {
  if (!AVG) {
    total1 =  0;
    total2 =  0;
    total3 =  0;
    for (i = 0; i < samples; i++) {
      total1 = total1 + cs_4_2.capacitiveSensorRaw(sampleres);
      total2 = total2 + cs_4_6.capacitiveSensorRaw(sampleres);
      total3 = total3 + cs_4_8.capacitiveSensorRaw(sampleres);
    }
    total1 = total1 / samples;
    total2 = total2 / samples;
    total3 = total3 / samples;
  } else {
    if (rsamplepos == rsamples) {
      rsamplepos = 0;
    }
    total1 = cs_4_2.capacitiveSensorRaw(sampleres);
    total2 = cs_4_6.capacitiveSensorRaw(sampleres);
    total3 = cs_4_8.capacitiveSensorRaw(sampleres);
    recordings[0][rsamplepos] = total1;
    recordings[1][rsamplepos] = total2;
    recordings[2][rsamplepos] = total3;
    total1 = 0;
    total2 = 0;
    total3 = 0;
    for (u = 0; u < rsamples; u++) {
      total1 = total1 + recordings[0][u];
    }
    for (u = 0; u < rsamples; u++) {
      total2 = total2 + recordings[1][u];
    }
    for (u = 0; u < rsamples; u++) {
      total3 = total3 + recordings[2][u];
    }
    total1 = total1 / rsamples;
    total2 = total2 / rsamples;
    total3 = total3 / rsamples;
    rsamplepos++;
  }

  if (!linear) {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (255 / 30) * sqrt(total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (255 / 30) * sqrt(total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (255 / 30) * sqrt(total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  } else {
    if ((total1 - cutoff - into1 - roff) > 0) {
      red = sensitivity * (700 / 255) * (total1 - cutoff - into1 - roff);
      if (red > 0xFF) {
        red = 0xFF;
      }
    } else {
      red = (byte)0x0;
    }

    if ((total2 - cutoff - into2 - goff) > 0) {
      green = sensitivity * (700 / 255) * (total2 - cutoff - into2 - goff);
      if (green > 0xFF) {
        green = 0xFF;
      }
    } else {
      green = (byte)0x0;
    }

    if ((total3 - cutoff - into3 - boff) > 0) {
      blue = sensitivity * (700 / 255) * (total3 - cutoff - into3 - boff);
      if (blue > 0xFF) {
        blue = 0xFF;
      }
    } else {
      blue = (byte)0x0;
    }
  }

  if (DEBUG) {
    if (!VISUALIZER) {
      if (((DEBUGALWAYSUPDATE) | (total1 - cutoff - into1 - roff) > 0) | ((total2 - cutoff - into2 - goff) > 0) | ((total3 - cutoff - into3 - boff) > 0)) {
        Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");

        Serial.print(cutoff + into1 + roff);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(cutoff + into2 + goff);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(cutoff + into3 + boff);
        Serial.print(total1 - cutoff - into1 - roff);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(total2 - cutoff - into2 - goff);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(total3 - cutoff - into3 - boff);
        Serial.print(total1);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(total2);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(total3);
        Serial.print(red);                  // print sensor output 1
        Serial.print("\t");
        Serial.print(green);                  // print sensor output 2
        Serial.print("\t");
        Serial.println(blue);
        if ((total1 - cutoff - into1 - roff) > 0) {
          Serial.print("R");
        } else {
          Serial.print(" ");
        }
        Serial.print("\t");
        if ((total2 - cutoff - into2 - goff) > 0) {
          Serial.print("Y");
        } else {
          Serial.print(" ");
        }
        Serial.print("\t");
        if (((int)(total3 - cutoff - into3 - boff)) > 0) {
          Serial.println("B");
        } else {
          Serial.println(" ");
        }
        Serial.print("\t");
        Serial.println(bright);
      }
    } else {
      int dbcdtemp1 = (1 * (total1 - cutoff - into1 - roff)) + 200; //10
      int dbcdtemp2 = (1 * (total2 - cutoff - into2 - goff)) + 200; //10
      int dbcdtemp3 = (1 * (total3 - cutoff - into3 - boff)) + 200; //10
      //int dbcdtemp = total1 - minamb1 + cutoff; //minamb and cutoff are both constants
      while ((dbcdtemp1 > -10) | (dbcdtemp2 > -10) | (dbcdtemp3 > -10)) {
        dbcdtemp1 = dbcdtemp1 - 10;
        dbcdtemp2 = dbcdtemp2 - 10;
        dbcdtemp3 = dbcdtemp3 - 10;
        if ((dbcdtemp1 > -10) & (dbcdtemp1 < 10)) {
          Serial.print("R");
        } else if ((dbcdtemp2 > -10) & (dbcdtemp2 < 10)) {
          Serial.print("Y");
        } else if ((dbcdtemp3 > -10) & (dbcdtemp3 < 10)) {
          Serial.print("B");
        } else {
          Serial.print(" ");
        }
      }
      Serial.println(" ");
    }
  }

  if (red > 0) {
    if (green > 0) {
      if (blue > 0) {
        //brown
        bright = ((bright) / 6);
        red = (byte)(((float)bright / 31) * (float)(0xFF - ((red + green + blue) / 3)));
        green = (byte)(red * ((float) 1 / 6));
        blue = (byte)0x00;
      } else {
        //orange
        red = (red + green) / 2;
        green = (green / 3);
        bright = ((bright) / 2);
        //bright = bright - ((0x1F / 0xFF) * (red + green) / 2);
      }
    } else if (blue > 0) {
      //purple
      bright = ((bright) / 2);
      //bright = bright - ((0x1F / 0xFF) * (red + blue) / 2);
    } else {
      //red
      bright = bright; //
    }
  } else if (green > 0) {
    if (blue > 0) {
      //green
      bright = ((bright) / 2);
      //bright = bright - ((0x1F / 0xFF) * (green + blue) / 2);
      green = (green + blue) / 2;
      red = (green - blue) / 2;
      if (red < 0) {
        red = 0;
      }
      if (red == 0) {
        blue = blue - green;
      } else {
        blue = 0;
      }
    } else {
      //yellow
      bright = bright; //
      red = green;//............... color
    }
  } else {
    //blue
    bright = bright; //
  }

  if (bright < 0) {
    bright = 0;
  }

  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(bright + 0xE0);
  SPI.transfer((byte)blue);
  SPI.transfer((byte)green);
  SPI.transfer((byte)red);


  recal();
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Demo -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void demo() {
  //int brightran = 0; //temporary brightness
  //int brightnltemp = 0;
  //int redtemp = 0;
  //int greentemp = 0;
  //int bluetemp = 0;
  //int seqplace = 0;
  //bool randrq = 1;
  //-=-=-=-=-=-=-=-=-=-=-=-=-=- SETUP
  if (randrq) {
    brightran = 1 + (rand() % (bright));
    randrq = 0;
    seqplace = rand() % (7);
    red = 0;
    green = 0;
    blue = 0;
    if ((demoseq[seqplace] == 1) | (demoseq[seqplace] == 5) | (demoseq[seqplace] == 6) | (demoseq[seqplace] == 7)) {
      red = rand() % 256;
    }
    if ((demoseq[seqplace] == 2) | (demoseq[seqplace] == 4) | (demoseq[seqplace] == 6) | (demoseq[seqplace] == 7)) {
      if ((demoseq[seqplace] == 6) | (demoseq[seqplace] == 7)) {
        blue = red;
      } else {
        blue = (rand() % 256);
      }
    }
    if ((demoseq[seqplace] == 3) | (demoseq[seqplace] == 4) | (demoseq[seqplace] == 5) | (demoseq[seqplace] == 7)) {
      if ((demoseq[seqplace] == 5) | (demoseq[seqplace] == 7)) {
        green = red;
      } else if (demoseq[seqplace] == 4) {
        green = blue;
      } else {
        green = (rand() % 256);
      }
    }
    //-=-=-=-=-=-=-=-=-=-=-=-=-=-
  } else {
    nltime++;
    if ((nltime > nltim) | (!nlbritimslw)) {
      nltime = 1;
      if ((!nlbritcutoff) | (bright > 0x07)) {
        if (brightnltemp > brightran) {
          brightnltemp--;
        } else if (brightnltemp < brightran) {
          brightnltemp++;
        } else {
          brightnltemp = brightnltemp;
          if (BRIPRE > 0) {
            brightran =  1 + (rand() % (BRIPRE));
          } else {
            brightran =  1 + (rand() % (bright));
          }
        }
      } else {
        brightnltemp = bright;
      }
    }

    if (redtemp > red) {
      redtemp--;
    } else if (redtemp < red) {
      redtemp++;
    } else {
      redtemp = redtemp;
    }
    if (bluetemp > blue) {
      bluetemp--;
    } else if (bluetemp < blue) {
      bluetemp++;
    } else {
      bluetemp = bluetemp;
    }
    if (greentemp > green) {
      greentemp--;
    } else if (greentemp < green) {
      greentemp++;
    } else {
      greentemp = greentemp;
    }
    if ((greentemp == green) & (bluetemp == blue) & (redtemp == red)) {
      randrq = 1;
    }
  }
  if (DEBUG) {
    if (!VISUALIZER) {
      Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
      if (seqplace == 0) {
        Serial.print("|");
      }
      Serial.print(demoseq[0]);
      Serial.print("\t");
      if (seqplace == 1) {
        Serial.print("|");
      }
      Serial.print(demoseq[1]);
      Serial.print("\t");
      if (seqplace == 2) {
        Serial.print("|");
      }
      Serial.print(demoseq[2]);
      Serial.print("\t");
      if (seqplace == 3) {
        Serial.print("|");
      }
      Serial.print(demoseq[3]);
      Serial.print("\t");
      if (seqplace == 4) {
        Serial.print("|");
      }
      Serial.print(demoseq[4]);
      Serial.print("\t");
      if (seqplace == 5) {
        Serial.print("|");
      }
      Serial.print(demoseq[5]);
      Serial.print("\t");
      if (seqplace == 6) {
        Serial.print("|");
      }
      Serial.print(demoseq[6]);
      Serial.print("\t");
      if (seqplace == 7) {
        Serial.print("|");
      }
      Serial.print(demoseq[7]);
      Serial.print("\t");
      if (seqplace == 8) {
        Serial.print("|");
      }
      Serial.print(demoseq[8]);
      Serial.print("\t");
      if (seqplace == 9) {
        Serial.print("|");
      }
      Serial.println(demoseq[9]);
      Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
      Serial.print(brightran);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(red);                  // print sensor output 2
      Serial.print("\t");
      Serial.print(green);
      Serial.print("\t");
      Serial.println(blue);
      Serial.print(brightnltemp);                  // print sensor output 1
      Serial.print("\t");
      Serial.print(redtemp);                  // print sensor output 2
      Serial.print("\t");
      Serial.print(greentemp);
      Serial.print("\t");
      Serial.println(bluetemp);
      Serial.println("-=-=-=-=-=-=-=-=-=-=-=-");
      Serial.print(nltim);
      Serial.print("\t");
      Serial.println(nltime);
    } else {
      int dbcdtemp1 = redtemp * 5; //10
      int dbcdtemp2 = greentemp * 5; //10
      int dbcdtemp3 = bluetemp * 5; //10
      //int dbcdtemp = total1 - minamb1 + cutoff; //minamb and cutoff are both constants
      while ((dbcdtemp1 > -10) | (dbcdtemp2 > -10) | (dbcdtemp3 > -10)) {
        dbcdtemp1 = dbcdtemp1 - 10;
        dbcdtemp2 = dbcdtemp2 - 10;
        dbcdtemp3 = dbcdtemp3 - 10;
        if ((dbcdtemp1 > -10) & (dbcdtemp1 < 10)) {
          Serial.print("R");
        } else if ((dbcdtemp2 > -10) & (dbcdtemp2 < 10)) {
          Serial.print("G");
        } else if ((dbcdtemp3 > -10) & (dbcdtemp3 < 10)) {
          Serial.print("B");
        } else {
          Serial.print(" ");
        }
      }
      Serial.println(" ");
    }
  }
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer((byte) brightnltemp + 0xE0);
  SPI.transfer((byte) bluetemp);
  SPI.transfer((unsigned byte) greentemp);
  SPI.transfer((byte) redtemp);
  delay(nlspeed);
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=- Sequence Capture -=-=-=-=-=-=-=-=-=-=-=-=-=-
void nlseqcapture() {
  //-=-=-=-=-=-=-=-=-=-=- Initialization
  int brighttemp = 0;
  unsigned long timeout = 0;
  int u = 0;

  for (u = 0; u < nlseqnumb; u++) {
    demoseq[u] = 0;
  }
  if (DEBUG) {
    Serial.print(bright);
  }
  //-=-=-=-=-=-=-=-=-=-=- Initial Animation
  while (brighttemp < bright) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer((brighttemp + 0xE0));
    SPI.transfer((byte)0x00);
    SPI.transfer((byte)0x00);
    SPI.transfer(0xEF);
    delay(50);
    brighttemp++;
  }
  while (brighttemp > 0) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer((brighttemp + 0xE0));
    SPI.transfer((byte)0x00);
    SPI.transfer(0xEF);
    SPI.transfer((byte)0x00);
    delay(30);
    brighttemp--;
  }
  while (brighttemp < bright) {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer((brighttemp + 0xE0));
    SPI.transfer(0xEF);
    SPI.transfer((byte)0x00);
    SPI.transfer((byte)0x00);
    delay(20);
    brighttemp++;
  }
  //-=-=-=-=-=-=-=-=-=-=- Initialization
  for (u = 0; u < nlseqnumb; u++) {
    timeout = millis();
    do {
      coloradd();
      if ((red == 0xFF) | (blue == 0xFF) | (green == 0xFF)) {
        for (int a = 0; a < 30; a++) {
          coloradd();
        }
      }
    } while ((millis() < (timeout + 1400)) & !((red == 0xFF) | (blue == 0xFF) | (green == 0xFF)));
    if (red == 0xFF) {
      if (blue == 0xFF) {
        if (green == 0xFF) {
          demoseq[u] = 7; //white
        } else {
          demoseq[u] = 6;//magenta
        }
      } else if (green == 0xFF) {
        demoseq[u] = 5; //yellow
      } else {
        demoseq[u] = 1;  //red
      }
    } else if (blue == 0xFF) {
      if (green == 0xFF) {
        demoseq[u] = 4; //cyan
      } else {
        demoseq[u] = 2; //blue
      }
    } else if (green == 0xFF) {
      demoseq[u] = 3; //green
    } else {
      demoseq[u] = 0; //black/error
      u = nlseqnumb;
    }
    brighttemp = 0;
    while (brighttemp < bright) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brighttemp + 0xE0);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      delay(30);
      brighttemp++;
    }
    brighttemp = 0;
    while (brighttemp < bright) {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brighttemp + 0xE0);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      SPI.transfer((byte)0xEF);
      delay(30);
      brighttemp++;
    }
    if (demoseq[u] == 0) {
      u = nlseqnumb;
    }
  }

  nlseqinputnumb = 0;
  timeout = 0; //now being used as a NULL check for sequence total
  for (u = 0; u < nlseqnumb; u++) {
    timeout = timeout + demoseq[u];
    if (demoseq[u] > 0) {
      nlseqinputnumb++;
    }
  }
  if (timeout == 0) {
    for (u = 1; u < 8; u++) {
      demoseq[u - 1] = u;
    }
    nlseqinputnumb = 7;
  }
}


//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Recalibrate -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void recal() {
  j++;
  if (DEBUG & !VISUALIZER) {
    Serial.println("-=-=-=-=-=- -=-=-=-=-=-");
    Serial.print(j);
    Serial.print("\t");
    Serial.println("");
  }
  if (total1 < (roff + into1)) {
    rtotal1 = rtotal1 + total1;
  } else {
    rtotal1 = rtotal1 + (rtotal1 / j);
  }
  if (total2 < (goff + into2)) {
    rtotal2 = rtotal2 + total2;
  } else {
    rtotal2 = rtotal2 + (rtotal2 / j);
  }
  if (total3 < (boff + into3)) {
    rtotal3 = rtotal3 + total3;
  } else {
    rtotal3 = rtotal3 + (rtotal3 / j);
  }
  if (j == (recalsamp - 1)) {
    j = 0;
    rtotal1 = rtotal1 / recalsamp;
    rtotal2 = rtotal2 / recalsamp;
    rtotal3 = rtotal3 / recalsamp;

    into1 = rtotal1 + (bleed * total2 / bleeddiv) + (bleed * total3 / bleeddiv);
    if (into1 < minamb1) {
      into1 = minamb1;
    }
    into2 = rtotal2 + (bleed * total1 / bleeddiv) + (bleed * total3 / bleeddiv);
    if (into2 < minamb2) {
      into2 = minamb2;
    }
    into3 = rtotal3 + (bleed * total2 / bleeddiv) + (bleed * total1 / bleeddiv);
    if (into3 < minamb3) {
      into3 = minamb3;
    }
  }
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Speed Test -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void speedtestfunction() {
  st++;
  if (st == 4) {
    st = 1;
  }
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  SPI.transfer(bright + 0xE0);
  if (st == 1) {
    SPI.transfer(0xFF);
  } else {
    SPI.transfer(0x00);
  }
  if (st == 2) {
    SPI.transfer(0xFF);
  } else {
    SPI.transfer(0x00);
  }
  if (st == 3) {
    SPI.transfer(0xFF);
  } else {
    SPI.transfer(0x00);
  }
}

//-=-=-=-=-=-=-=-=-=-=-=-=-=-=- Power Down -=-=-=-=-=-=-=-=-=-=-=-=-=-=-
void powerdown() {
  brightpd = bright;
  redpd = red;
  bluepd = blue;
  greenpd = green;
  if (DEBUG) {
    Serial.println("Power Down");
  }
  if ((!digitalRead(mode2pin)) | (MODE == 2)) {
    brightpd = brightnltemp;
    redpd = redtemp;
    bluepd = bluetemp;
    greenpd = greentemp;
  }
  do {
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(0x00);
    SPI.transfer(brightpd + 0xE0);
    SPI.transfer(bluepd);
    SPI.transfer(greenpd);
    SPI.transfer(redpd);
    if (DEBUG) {
      Serial.print("down -");
      Serial.print("\t");
      Serial.println(brightpd);
    }
    delayMicroseconds(10000000);
  } while (brightpd-- > 0);
  do {
    delayMicroseconds(300000);
  } while (!digitalRead(powerpin));
  delayMicroseconds(10000000);
  into1 = cs_4_2.capacitiveSensor(sampleres);
  into2 = cs_4_6.capacitiveSensor(sampleres);
  into3 = cs_4_8.capacitiveSensor(sampleres);
  brightpd++;
  if ((!digitalRead(mode2pin)) | (MODE == 2)) {
    do {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brightpd + 0xE0);
      SPI.transfer(bluepd);
      SPI.transfer(greenpd);
      SPI.transfer(redpd);
      if (DEBUG) {
        Serial.print("up -");
        Serial.print("\t");
        Serial.println(brightpd);
      }
      delayMicroseconds(10000000);
    } while (brightpd++ < brightnltemp);
  } else {
    do {
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(0x00);
      SPI.transfer(brightpd + 0xE0);
      SPI.transfer(bluepd);
      SPI.transfer(greenpd);
      SPI.transfer(redpd);
      if (DEBUG) {
        Serial.print("up -");
        Serial.print("\t");
        Serial.println(brightpd);
      }
      delayMicroseconds(10000000);
    } while (brightpd++ < bright);
  }
}
